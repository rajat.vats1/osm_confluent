
# PRE-REQUISITE

  - Java should be installed in target nodes
  - dns should be set for kafka servers

# VARS
  - scala version
  - kafka version
  - kafka_user
  - kafka_url (for downloading kafka)
  - kafka_dns [list variable] (dns of each kafka nodes)

# HOST
  - In hosts file , server no should be defined for each host like server_no=1

# HOW TO RUN

  - by default ansible-playbook -i hosts site.yml
  - ansible-playbook -i hosts --extra-vars '{"kafka_dns":["a.com","b.com","cl.com"]}' site.yml
